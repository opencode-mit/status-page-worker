type MonitorTest = {
	timestamp: number;
	duration: number;
	successful: boolean;
};

type MonitorStatus = {
	checks: MonitorTest[];
} & MonitorConfig;

type MonitorConfig = {
	id: string;
	title: string;
	displayedURL: string;
	testURL: string;
};

const CONFIG: MonitorConfig[] = [
	{
		id: 'fuiz',
		title: 'Fuiz Website',
		displayedURL: 'https://fuiz.org',
		testURL: 'https://fuiz.org',
	},
	{
		id: 'fuiz_api',
		title: 'Fuiz API',
		displayedURL: 'https://api.fuiz.org',
		testURL: 'https://api.fuiz.org/hello',
	},
	{
		id: 'corkboard_api',
		title: 'Corkboard API',
		displayedURL: 'https://corkboard.fuiz.org',
		testURL: 'https://corkboard.fuiz.org/hello',
	},
];

async function bring(url: string): Promise<[boolean, number]> {
	let timestamp = performance.now();
	try {
		let resp = await fetch(url);
		return [resp.ok, performance.now() - timestamp];
	} catch (e) {
		return [false, performance.now() - timestamp];
	}
}

async function testURL(url: string, remainingAttempts = 3): Promise<[boolean, number][]> {
	if (remainingAttempts == 0) return [];
	return [await bring(url), ...(await testURL(url, remainingAttempts - 1))];
}

function average(...values: number[]): number {
	return values.reduce((s, c) => s + c, 0) / values.length;
}

export interface Env {
	// Example binding to KV. Learn more at https://developers.cloudflare.com/workers/runtime-apis/kv/
	CLOUDPAGE: KVNamespace;
}

export default {
	// The scheduled handler is invoked at the interval set in our wrangler.toml's
	// [[triggers]] configuration.
	async scheduled(event: ScheduledEvent, env: Env, ctx: ExecutionContext): Promise<void> {
		let status = (await env.CLOUDPAGE.get<MonitorStatus[]>('monitors', 'json')) || [];

		function is_in_last_30_days(monitor: MonitorTest): boolean {
			const MILLIS_IN_DAY = 24 * 60 * 60 * 1000;
			return Date.now() - monitor.timestamp <= 30 * MILLIS_IN_DAY;
		}

		status = CONFIG.map((c) => ({
			id: c.id,
			title: c.title,
			displayedURL: c.displayedURL,
			testURL: c.testURL,
			checks: status.find((s) => s.id === c.id)?.checks.filter(is_in_last_30_days) || [],
		}));

		status = await Promise.all(
			status.map(async (s) => {
				const res = await testURL(s.testURL);

				const successful = res.some(([status]) => status);
				const duration = average(...res.filter(([status]) => status === successful).map(([, duration]) => duration));

				return {
					...s,
					checks: [
						...s.checks,
						{
							successful,
							duration,
							timestamp: Date.now(),
						},
					],
				};
			})
		);

		await env.CLOUDPAGE.put('monitors', JSON.stringify(status));
	},
};
